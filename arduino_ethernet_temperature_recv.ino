#include <HttpClient.h>
#include <b64.h>

#include <VirtualWire.h> // use Virtual library for decode signal from Rx module
#include <SPI.h>
#include <Ethernet.h>
#include <HttpClient.h>
#include <EthernetClient.h>

#define RCVPIN 9
#define DEBUG 1

#if DEBUG
static char debug_buf[64];
#endif

/*
 * Ethernet connection
 */

byte mac[] = {  0x36, 0x44, 0x42, 0x12, 0x24, 0x16 };
IPAddress sensor_collector(10, 42, 69, 8);
int localPort = 64258;
EthernetClient ethClient;

const char serverName[] = "10.42.69.8";

/*
 * Sensor receivers
 */
#define SENSOR_OUTSIDE 0x1

struct TxMsg {
  uint8_t sensor_id;
  int16_t humidity;
  int16_t temp;
  int16_t heat_index;
};

static uint8_t recvMsgSize = 0;
static int httpClientTimeout = 30 * 1000;
static int httpClientNetworkDelay = 1000;

void push_sensor_data(const TxMsg &txMsg)
{
  int bodyLen = 0;
  unsigned long timeoutStart = 0;

   #if DEBUG
    Serial.print("Connecting to: ");
    Serial.println(serverName);
  #endif
  
  HttpClient http(ethClient);
  Serial.println("coucou2.5");
  int err = http.get(sensor_collector, serverName, 80, "/test?q=arduino");
  Serial.println("coucou3");
  if (err != 0) {
    #if DEBUG
    Serial.print("Failed to connect: ");
    Serial.println(err);
    #endif
    goto stophttp;
  }

  #if DEBUG
  Serial.println("startedRequest ok");
  #endif
  err = http.responseStatusCode();
  if (err < 0) {
    goto stophttp;
  }

    #if DEBUG
  Serial.print("Got status code: ");
  Serial.println(err);
  #endif

  err = http.skipResponseHeaders();
  if (err < 0) {
    #if DEBUG
    Serial.print("Failed to skip response headers: ");
    Serial.println(err);
    #endif
    goto stophttp;
  }

  bodyLen = http.contentLength();
  Serial.print("Content length is: ");
  Serial.println(bodyLen);
  Serial.println();
  Serial.println("Body returned follows:");
  timeoutStart = millis();
  char c;
  // Whilst we haven't timed out & haven't reached the end of the body
  while ( (http.connected() || http.available()) &&
         ((millis() - timeoutStart) < httpClientTimeout) ) {
      if (http.available()) {
          c = http.read();
          // Print out this character
          Serial.print(c);
         
          bodyLen--;
          // We read something, reset the timeout counter
          timeoutStart = millis();
      } else {
          // We haven't got any data, so let's pause to allow some to
          // arrive
          delay(httpClientNetworkDelay);
      }
  }

stophttp:
  http.stop();
}
void setup() 
{
#if DEBUG
  Serial.begin(9600);
  Serial.println("Initializing ethernet...");
#endif
  // VirtualWire 
  // Bits per sec
  vw_setup(2000);
  // set pin for connect receiver module 
  vw_set_rx_pin(RCVPIN);  
  // Start the receiver PLL running
  vw_rx_start();

  delay(1000);

  #if DEBUG
    Serial.println("VirtualWire inited.");
  #endif

  // Ethernet must be inited after virtualwire
  // else it will not work
  while (Ethernet.begin(mac) != 1) {
    #if DEBUG
    Serial.println("Error getting IP address via DHCP, trying again...");
    #endif
    delay(5000);
  }

  delay(1000);

  #if DEBUG
    Serial.print("Ethernet initialized. IP: ");
    Serial.println(Ethernet.localIP());
    Serial.println("Initialize virtualwire");
  #endif
}

void loop()
{
  uint8_t buf[VW_MAX_MESSAGE_LEN];
  uint8_t buflen = VW_MAX_MESSAGE_LEN;
  if (vw_have_message() && vw_get_message(buf, &buflen)) {
    // ignore if, strangely empty
    if (buflen == 0) {
      #if DEBUG
      Serial.print("Strangely empty buffer, ignoring");
      #endif
      return;
    }
    memcpy(&recvMsgSize, &buf[0], sizeof(recvMsgSize));
    #if DEBUG
    sprintf(debug_buf, "msg recv len: %d. payload size: %d\n", buflen, recvMsgSize);
    Serial.print(debug_buf);
    #endif

    // If payload size is not payload - payload len, it's not a valid thing.
    if (recvMsgSize != (buflen - 1)) {
      #if DEBUG
      snprintf(debug_buf, sizeof(debug_buf), "Payload size is not valid. Received raw message doesn't correspond (%d != %d).\n", (buflen - 1), recvMsgSize);
      Serial.print(debug_buf);
      #endif
      return;
    }

    // If payload is not a standard TxMsg, ignore it
    if (recvMsgSize != sizeof(TxMsg)) {
      return;
    }

    TxMsg msg;
    memcpy(&msg, &buf[1], sizeof(TxMsg));
    
    #if DEBUG
      sprintf(debug_buf, "[Sensor ID %d] Hum: %d, temp: %d°C, heat idx: %d\n", msg.sensor_id, msg.humidity, msg.temp, msg.heat_index);
      Serial.print(debug_buf);
    #endif

    push_sensor_data(msg);
  }
}

