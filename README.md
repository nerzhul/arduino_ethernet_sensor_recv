# Arduino Ethernet monitoring sensor

This Arduino Ethernet monitoring sensor aims to collect sensor data over RFC433Mhz receiver.

Currently supported sensors are:

* DHT22/DHT11 temperature & humidity sensor ![source code](https://gitlab.com/nerzhul/arduino_uno_dht22_sensor)
